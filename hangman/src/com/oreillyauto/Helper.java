package com.oreillyauto;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;


public class Helper {

    public static void loadWordsIntoMemory(List<String> wordList) {

        /**
         * Load words into memory from execution path (JDK 8)
         */
        try (Stream<String> stream = Files.lines(Paths.get("words.txt"))) {
            stream.forEach(word -> wordList.add(word.trim()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * Load words into memory using package path (JDK 7)
         */
        /*try {            
            URL url = Hangman.class.getClassLoader().getResource("com/oreillyauto/com/data/words.txt");
            BufferedInputStream inputStream = new BufferedInputStream(new URL(url.toString()).openStream());
            
            // Load words into memory using package path       
            try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.trim().length() > 0) {
                        wordList.add(line.trim());
                    }
                }
            }                    
        } catch (Exception e) {
            System.out.println("File/Stream Error!");
            e.printStackTrace();
        }*/
    }

    public static void populateWordUnderscores(List<String> wordList, 
            Integer wordListIndex, Hangman hangman, List<Character> correctCharList) {
        
        if (wordList.size() == 0) {
            hangman.setMessage("Error: Word List Is Empty!!!!");
        } else {
            // Build the underscores and characters
        	// For example: the word "intern" should look like "_ _ _ _ _ _"
            StringBuilder sb = new StringBuilder();
            sb.append("_ _ _ _ _ _ (Incomplete Method)");
            
            //... 
            
            hangman.setWordUnderscores(sb.toString());
        }
    }

    public static void setMessage(String option, boolean gameOver, String currentWord, 
    		Hangman hangman) {
        // Set a user message based on the current user selected option
        // e.g. hangman.setMessage("");
        
        if (option.length() == 0) {
            
        } else {
            // Manage Numeric Options
            if (isNumeric(option)) {
  
            } else {
                // Manage Non-Numeric Options
              
            }
        }
    }

    private static boolean isNumeric(String option) {
        try {
            Integer.parseInt(option);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void clearTheConsole() {
        try {
            Thread.sleep(500);
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object getUserOption(BufferedReader consoleReader) {
        try {
            String response = consoleReader.readLine();
            
            if (response != null) {
                return (Helper.isNumeric(response)) ? Integer.parseInt(response) : response;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}

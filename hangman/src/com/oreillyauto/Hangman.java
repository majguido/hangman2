package com.oreillyauto;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Your task is to complete the game "Hangman."
 * - You have words.txt and word-v2.txt to use as a wordlist.
 * - You can build your own word file if you want.
 * - Reference Helper.java as some of the helper (stubbed) functions 
 *   will assist (speed up) your development.
 */

public class Hangman {    
    private static final String HANGMAN_LEGS = "legs";
    private static final String HANGMAN_LOWERTORSO = "lowertorso";
    private static final String HANGMAN_UPPERTORSO = "uppertorso";
    private static final String HANGMAN_HEAD = "head";
    public boolean gameOver = false;
    public static BufferedReader consoleReader;
    public List<String> wordList = new ArrayList<String>();
    public int wordListIndex = 0;              // Position in word list
    public int attempts = 0;                   // Number of tries/attempts
    public int fails = 0;                      // Number of fails
    public String message = "";                // Message to the user 
    public String currentWord = "";            // Current hangman word
    public String wordUnderscores = "";        // Underscore and letters string
    public String option = "";                 // Current selected user option
    public List<Character> correctCharList;    // Correct character guesses
    public List<Character> incorrectCharList;  // Incorrect character guesses
    public Hangman hangman;
    
    public Hangman() {
    	// Declare Variables
    	// Get access to the Hangman() we created via "this" because
    	// we pass the hangman object to the Helper class in some cases.
        hangman = this; 
        
        // BufferedReader is synchronous while Scanner is not. 
        // BufferedReader has significantly larger buffer memory than Scanner.
        consoleReader = new BufferedReader(new InputStreamReader(System.in)); 
        
        // INITIALIZE YOUR 2 LISTS HERE!!!!!!!!!!!!!!!!!
        // (correctCharList and incorrectCharList)
        
        
        // Load the words from the word file into memory
        Helper.loadWordsIntoMemory(wordList);

        // Loop - get user selection and respond
        while (true) {
            // Draw the board (menu, hangman area, underscores)
            drawBoard();
            
            // Get the user selection
            Object userSelection = Helper.getUserOption(consoleReader);
            
            // Process the user selection
            if (userSelection instanceof Integer) {
                processNumericSelection((Integer)userSelection);
            } else {
                processAlphaSelection((String)userSelection);
            }
        }
    }

    /**
     * In this method, we need to figure out what the user wants
     * to do with the menu option that they selected. Notice that 
     * option is a global variable. We can assess what to do for 
     * a given input based on the option selected.
     * @param userSelection
     */
    private void processNumericSelection(Integer userSelection) {
        
        switch (userSelection) {
            case 1:
                // Submit a letter
                option = "1";
                break;
            case 2:
                // Guess the word
                option = "2";
                break;
            case 3:
                // Give up
                option = "3";
                break;
            case 4:
                // Quit
                option = "4";
                System.out.println("Goodbye!");
                System.exit(0);
                break;
            default:
                message = ""; // set error message (for invalid selection)
        }
        
    }

    private void processAlphaSelection(String userSelection) {
        switch (option) {
            case "1": // User selected option 1 (guess a letter)
                char c = ' '; // Get the character!
                processLetterChoice(c);
                break;
            case "2": // User selected option 2 (guess the word)
                processWordGuess(userSelection);
                break;
            default :
                System.out.println("Invalid Selection!");
                break;
        }
    }

    private void processWordGuess(String userSelection) {
        // Check if word is correct
        // Increment counters
    }

    private void processLetterChoice(char c) {
        // Increment counters
        // see if letter is correct
        // update lists
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public void setWordUnderscores(String wordUnderscores) {
        this.wordUnderscores = wordUnderscores;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
    }
    
    private void drawBoard() {
        // Clear the console, set messages in context, build the underscore string
        Helper.clearTheConsole();
        Helper.setMessage(option, gameOver, currentWord, hangman);
        Helper.populateWordUnderscores(wordList, wordListIndex, hangman, correctCharList);
        
        // Do not remove. The clearTheConsole call adds an 
        // odd character in Eclipse. We will build a new line
        System.out.println("");
        
        // By default, the entire hangman is drawn to the console.
        // You will need to remove the actual drawing of the man.
        // Notice the numeric spaces needed to draw the pole/man.
        // You may see 2 backslashes to draw an arm or a leg. This is
        // necessary as we need to escape the backslash with a backslash
        // in order to draw it properly in the console.
        System.out.println("******* Hangman 1.0 *******    |----");
        // (31 spaces to the pole and 35 spaces to the head)
        System.out.print  ("Options:                       |   O"); // O   "); // 31-35 
        drawHangman(HANGMAN_HEAD);
        // (31 spaces to the pole and 34 spaces to the left arm)
        System.out.print  ("1. Submit a letter             |  /|\\"); // /|\\ "); // 31-34 
        drawHangman(HANGMAN_UPPERTORSO);
        System.out.print  ("2. Guess the word              |   |"); // |   "); // 31-35
        drawHangman(HANGMAN_LOWERTORSO);
        System.out.print  ("3. Give up on this word        |  / \\"); // / \\ "); // 31-34
        drawHangman(HANGMAN_LEGS);
        System.out.println("4. Quit                      __|__     " + wordUnderscores);
        System.out.println("");
        System.out.println(message);
        
        // Clear the message after we show it
        message = "";
        
        // Need to build a new board to handle 2 new options
        // once the user wins or loses
        // 1. Play Again
        // 2. Quit
    }

    /**
     * Draw Hangman Body Parts
     * The number of fails is global so we can use this information
     * in order to know how to draw the hangman.
     * @param bodyPart
     */
    private void drawHangman(String bodyPart) {
        switch(bodyPart) {
            case HANGMAN_HEAD:
                System.out.println("");
                break;
            case HANGMAN_UPPERTORSO:
                switch(fails) {
                     
                }
                System.out.println("");
                break;
            case HANGMAN_LOWERTORSO:
                System.out.println("");
                break;
            case HANGMAN_LEGS:
                switch(fails) {
                }
                System.out.println("");
                break;
        }
    }

    public static void main(String[] args) {
        new Hangman();
    }

}
